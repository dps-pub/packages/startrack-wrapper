<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStarTrackTables extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::connection(config('startrack.database'))->create('StarTrack_Orders', function(Blueprint $t)
        {
            $t->increments('id')->unsigned();
            $t->string('order_id');
            $t->string('order_reference');

            $t->decimal('total_cost', 5, 2)->nullable();
            $t->decimal('total_gst', 5, 2)->nullable();
            $t->string('status')->nullable();
            $t->string('tracking_status')->nullable();

            $t->dateTime('order_creation_date')->nullable();
            $t->softDeletes();
            $t->timestamps();
        });

        Schema::connection(config('startrack.database'))->create('StarTrack_Shipments', function(Blueprint $t)
        {
            $t->increments('id')->unsigned();
            $t->string('shipment_id')->nullable();
            $t->integer('order_id')->nullable();
            $t->integer('label_id')->nullable();

            $t->decimal('total_cost', 5, 2)->nullable();
            $t->decimal('total_gst', 5, 2)->nullable();
            $t->string('status')->nullable();
            $t->string('tracking_status')->nullable();
            $t->dateTime('tracking_at')->nullable();

            $t->dateTime('despatch_date')->nullable();
            $t->string('sender_references')->nullable();

            $t->string('shipment_reference', 50);
            $t->integer('address_from_id')->nullable();
            $t->integer('address_to_id')->nullable();

            $t->softDeletes();
            $t->timestamps();
        });

        Schema::connection(config('startrack.database'))->create('StarTrack_Items', function(Blueprint $t)
        {
            $t->increments('id')->unsigned();
            $t->integer('shipment_id')->nullable();

            $t->string('item_id');
            $t->string('article_id');
            $t->string('consignment_id');
            $t->string('tracking_status')->nullable();
            $t->dateTime('tracking_at')->nullable();

            $t->string('status')->nullable();

            $t->string('item_reference', 50);
            $t->string('product_id', 5);
            $t->string('invoice_number', 35)->nullable();
            $t->enum('packaging_type', ['CTN', 'PAL', 'SAT', 'BAG', 'ENV', 'ITM', 'JIF', 'SKI'])->nullable();
            $t->decimal('length', 5, 2)->nullable();
            $t->string('height', 5, 2)->nullable();
            $t->string('width', 5, 2)->nullable();
            $t->string('weight', 5, 2)->nullable();

            $t->softDeletes();
            $t->timestamps();
        });

        Schema::connection(config('startrack.database'))->create('StarTrack_Addresses', function(Blueprint $t)
        {
            $t->increments('id')->unsigned();
            $t->string('name', 40);
            $t->string('business_name', 40);
            $t->string('line_1', 40);
            $t->string('line_2', 40)->nullable();
            $t->string('line_3', 40)->nullable();
            $t->string('suburb', 30);
            $t->string('state', 3);
            $t->string('postcode', 4);
            $t->string('phone', 20)->nullable();
            $t->string('email', 50)->nullable();
            $t->string('delivery_instructions', 60)->nullable();

            $t->softDeletes();
            $t->timestamps();
        });

        Schema::connection(config('startrack.database'))->create('StarTrack_Labels', function(Blueprint $t)
        {
            $t->increments('id')->unsigned();
            $t->string('request_id');
            $t->string('status')->nullable();
            $t->string('url')->nullable();

            $t->softDeletes();
            $t->timestamps();
        });

        Schema::connection(config('startrack.database'))->create('StarTrack_Settings', function(Blueprint $t)
        {
            $t->increments('id')->unsigned();
            $t->string('name');
            $t->string('data')->nullable();

            $t->softDeletes();
            $t->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::connection(config('startrack.database'))->drop('StarTrack_Shipments');
        Schema::connection(config('startrack.database'))->drop('StarTrack_Orders');
        Schema::connection(config('startrack.database'))->drop('StarTrack_Items');
        Schema::connection(config('startrack.database'))->drop('StarTrack_Addresses');
        Schema::connection(config('startrack.database'))->drop('StarTrack_Labels');
        Schema::connection(config('startrack.database'))->drop('StarTrack_Settings');
    }
}