<?php namespace DPS\StarTrack;

class StarTrackServiceProvider extends \Illuminate\Support\ServiceProvider
{
    protected $defer = true;

    public function boot()
    {
        $this->publishes([
            __DIR__.'/config/startrack.php' => config_path('startrack.php'),
        ]);

        $this->publishes([
            __DIR__.'/Migrations/' => database_path('migrations')
        ], 'migrations');
    }

    public function register()
    {
        $this->mergeConfigFrom(__DIR__.'/config/startrack.php', 'startrack');

        $this->app->singleton(StarTrack::class, function ($app) {
            return new StarTrack();
        });
    }

    public function provides()
    {
        return [StarTrack::class];
    }
}
