<?php namespace DPS\StarTrack\Exceptions;

use Throwable;
use Illuminate\Support\MessageBag;

class StarTrackException extends \Exception
{
    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        if($message instanceof MessageBag){
            $message = $message->first();
        } else {
            if(!empty((array)json_decode($message))){
                $message = json_decode($message);
                $allMessages = "";

                foreach($message->errors as $error){
                    $allMessages .= $error->message.(isset($error->field) ? " - ".$error->field : '')."\n";
                }

                $message = $allMessages;
            }
        }

        parent::__construct($message, $code, $previous);
    }
}