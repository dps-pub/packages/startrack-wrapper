<?php namespace DPS\StarTrack\Controllers;

use DPS\StarTrack\Exceptions\StarTrackException;

class Controller
{
    public $account_number;

    private $host = "digitalapi.auspost.com.au";
    private $debug = false;
    private $key;
    private $secret;

    const PUT = "PUT";
    const GET = "GET";
    const POST = "POST";
    const DELETE = "DELETE";

    function __construct()
    {
        if(\App::environment('production') || !config('startrack.debug')){
            // $this->host .= "/api";
        } else{
            $this->debug = true;
            $this->host .= "/test";
            $this->account_number = config('startrack.account_number');
        }

        $this->host .= "/shipping/v1/";
        $this->account_number = $this->debug ? $this->account_number : config('startrack.account_number');
        $this->key = config('startrack.api_key');
        $this->secret = config('startrack.secret');
    }

    /**
     * @param $type
     * @param $url
     * @param string|null $content
     * @param bool $special - if special request
     * @return bool|mixed
     * @throws StarTrackException
     */
    protected function request($type, $url, $content = '', $special = false)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->getHeaders($content));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $content);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $type);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $this->buildUrl($url));
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if($this->validResponseCode($code)){
            logger()->critical('star track exception', [
                'api_url' => $this->buildUrl($url),
                'request_type' => $type,
                'headers' => $this->getHeaders($content),
                'content' => $content,
                'response' => $response,
            ]);

            throw new StarTrackException($response, $code);
        }

        if($special){
            return $response;
        }

        if($response){
            $response = json_decode($response);
            $response = empty((array)$response) ? null : $response;
        }

        return $response ?: true;
    }

    private function getAuthHeader()
    {
        return base64_encode($this->key.":".$this->secret);
    }

    private function buildUrl($url)
    {
        return 'https://'.$this->host.$url;
    }

    private function getHeaders($content)
    {
        $headers = [
            'Authorization: Basic '.$this->getAuthHeader(),
            'Host: digitalapi.auspost.com.au',
        ];

        if(!is_array($content) && strlen($content) > 0){
            $headers[] = 'Content-Type: application/json';
            $headers[] = 'Content-Length: '.strlen($content);
        }

        return array_merge($headers, [
            'Accept: */*',
            'Account-Number: '.$this->account_number,
            'Cache-Control: no-cache',
            'Connection: close',
        ]);
    }

    private function validResponseCode($code): bool
    {
        return !((int)$code >= 200 && (int)$code < 300);
    }
}
