<?php namespace DPS\StarTrack\Controllers;

use DPS\StarTrack\Exceptions\StarTrackException;
use DPS\StarTrack\Models\Address;
use DPS\StarTrack\Models\Item;
use DPS\StarTrack\Models\Price;
use DPS\StarTrack\Models\Shipment;

class ShipmentController extends Controller
{
    /**
     * @param array|string $ids
     * @return bool
     */
    public function delete($ids)
    {
        if(is_array($ids)){
            $ids = implode(',', $ids);
        }

        $result = $this->request(self::DELETE, "shipments/{$ids}");

        if($result){
            $shipments = Shipment::whereIn('shipment_id', explode(',', $ids))->with('to', 'from', 'items')->get();
            $shipments->each(function($shipment){
                $shipment->to->delete();
                $shipment->from->delete();
                $shipment->items->each->delete();
            });

            $shipments->each->delete();
        }

        return $result;
    }

    /**
     * @param Shipment[]|Shipment $shipments
     * @return Shipment[]|Shipment
     * @throws StarTrackException
     */
    public function create($shipments)
    {
        if(!is_array($shipments)){
            $shipments = [$shipments];
        }

        foreach($shipments as $shipment) {
            $shipment->formatForRequest();
        }

        $body = (object)[];
        $body->shipments = $shipments;
        $body = json_encode($body);

        $response = $this->request(self::POST, "shipments", $body);
        $results = [];

        foreach($response->shipments as $shipmentResponse){

            $shipment = new Shipment();
            $shipment->shipment_id = $shipmentResponse->shipment_id;
            $shipment->shipment_reference = $shipmentResponse->shipment_reference;
            $shipment->total_cost = isset($shipmentResponse->shipment_summary) ? isset($shipmentResponse->shipment_summary->total_cost) ? $shipmentResponse->shipment_summary->total_cost : null : null;
            $shipment->total_gst = isset($shipmentResponse->shipment_summary) ? isset($shipmentResponse->shipment_summary->total_gst) ? $shipmentResponse->shipment_summary->total_gst : null : null;
            $shipment->status = $shipmentResponse->shipment_summary->status;
            $shipment->save();

            foreach($shipmentResponse->items as $itemResponse){

                $item = new Item();
                $item->item_id = $itemResponse->item_id;
                $item->item_reference = $itemResponse->item_reference;
                $item->article_id = $itemResponse->tracking_details->article_id;
                $item->consignment_id = $itemResponse->tracking_details->consignment_id;
                $item->product_id = $itemResponse->product_id;
                $item->status = $itemResponse->item_summary->status;
                $item->shipment_id = $shipment->id;
                $item->save();

            }

            $results[] = $shipment->fresh('items');
        }

        return count($results) == 1 ? $results[0] : $results;
    }

    /**
     * @param array|null $ids
     * @param int $offset
     * @param int $amount
     * @param array|null $reference
     * @return array
     */
    public function get(array $ids = null, $offset = 0, $amount = 100, array $reference = null)
    {
        $url = "shipments?offset={$offset}&number_of_shipments={$amount}";

        if($ids){
            $ids = implode(',', $ids);
            $url .= "&shipment_ids={$ids}";
        }

        if($reference){
            $reference = implode(",", $reference);
            $url .= "&sender_reference={$reference}";
        }

        $response = $this->request(self::GET, $url);
        $orders = [];

        foreach($response->shipments as $shipmentResponse){

            $shipment = Shipment::where('shipment_id', $shipmentResponse->shipment_id)->first();
            if(!$shipment) $shipment = new Shipment();

            $shipment->shipment_id = $shipmentResponse->shipment_id;
            $shipment->shipment_reference = $shipmentResponse->shipment_reference;
            $shipment->created_at = \Carbon::createFromFormat('Y-m-d\TH:i:sT', $shipmentResponse->shipment_creation_date);
            $shipment->total_cost = $shipmentResponse->shipment_summary->total_cost;
            $shipment->total_gst = $shipmentResponse->shipment_summary->total_gst;
            $shipment->status = $shipmentResponse->shipment_summary->status;

            if(!$shipment->hasAddresses()){
                $to = new Address();
                $to->line_1 = $shipmentResponse->to->lines[0];
                $to->line_2 = isset($shipmentResponse->to->lines[1]) ? $shipmentResponse->to->lines[1] : null;
                $to->suburb = $shipmentResponse->to->suburb;
                $to->postcode = $shipmentResponse->to->postcode;
                $to->state = $shipmentResponse->to->state;
                $to->name = $shipmentResponse->to->name;
                $to->email = isset($shipmentResponse->to->email) ? $shipmentResponse->to->email : null;
                $to->phone = isset($shipmentResponse->to->phone) ? $shipmentResponse->to->phone : null;
                $to->save();

                $from = new Address();
                $from->line_1 = $shipmentResponse->from->lines[0];
                $from->line_2 = isset($shipmentResponse->from->lines[1]) ? $shipmentResponse->from->lines[1] : null;
                $from->suburb = $shipmentResponse->from->suburb;
                $from->postcode = $shipmentResponse->from->postcode;
                $from->state = $shipmentResponse->from->state;
                $from->name = $shipmentResponse->from->name;
                $from->email = isset($shipmentResponse->from->email) ? $shipmentResponse->from->email : null;
                $from->phone = isset($shipmentResponse->from->phone) ? $shipmentResponse->from->phone : null;
                $from->save();

                $shipment->address_from_id = $from->id;
                $shipment->address_to_id = $to->id;
            }

            if($shipment->isVeryDirty()){
                $shipment->save();
            }

            foreach($shipmentResponse->items as $itemResponse){
                $item = Item::where('item_id', $itemResponse->item_id)->first();
                if(!$item) $item = new Item();

                $item->item_id = $itemResponse->item_id;
                $item->item_reference = $itemResponse->item_reference;
                $item->article_id = $itemResponse->tracking_details->article_id;
                $item->consignment_id = $itemResponse->tracking_details->consignment_id;
                $item->product_id = $itemResponse->product_id;
//                $item->total_cost = $itemResponse->item_summary->total_cost;
//                $item->total_gst = $itemResponse->item_summary->total_gst;
                $item->status = $itemResponse->item_summary->status;
                $item->shipment_id = $shipment->id;

                if($item->isVeryDirty()){
                    $item->save();
                }
            }

            $orders[] = $shipment->fresh(['items', 'to', 'from']);
        }

        return $orders;
    }

    /**
     * @param $id
     * @param Item[] $items
     * @return bool
     * @throws StarTrackException
     */
    public function update($id, $items)
    {
        foreach($items as $item){
            $item->rules["item_id"] = "required";
            $item->formatForRequest();
        }

        $body = (object)[];
        $body->items = $items;
        $body = json_encode($body);

        return $this->request(self::PUT, "shipments/{$id}/items", $body);
    }

    /**
     * @param Price $price
     * @return bool|mixed
     *
     * https://developers.auspost.com.au/apis/shipping-and-tracking/reference/get-item-prices
     */
    public function price(Price $price)
    {
        return $this->request(self::POST, "prices/items", $price->formatForRequest());
    }
}