<?php
/**
 * Created by PhpStorm.
 * User: benjamin
 * Date: 13/11/17
 * Time: 3:51 PM
 */

namespace DPS\StarTrack\Controllers;


use DPS\StarTrack\Exceptions\StarTrackException;

class PostageController
{
    private $host = "digitalapi.auspost.com.au";
    private $key;

    const GET = "GET";

    public function __construct()
    {
        $this->key = config('startrack.postage_api_key');
    }

    public function search($query)
    {
        return $this->request(self::GET, '/postcode/search.json?q='.urlencode($query));
    }

    public function request($type, $uri, $content = [], $special = false)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->getHeaders());
        curl_setopt($ch, CURLOPT_POSTFIELDS, $content);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $type);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $this->buildUrl($uri));
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);

        $response = curl_exec($ch);
        $code = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        curl_close($ch);

        if($this->validResponseCode($code)){
            throw new \Exception($response, $code);
        }

        if($special){
            return $response;
        }

        if($response){
            $response = json_decode($response);
            $response = empty((array)$response) ? null : $response;
        }

        return $response ?: true;
    }

    private function validResponseCode($code): bool
    {
        return !((int)$code >= 200 && (int)$code < 300);
    }

    public function getHeaders()
    {
        return [
            'Accept: */*',
            'Cache-Control: no-cache',
            'Connection: close',
            'auth-key: '.$this->key,
            'Host: digitalapi.auspost.com.au',
        ];
    }

    public function buildUrl($uri)
    {
        return 'https://'.$this->host.$uri;
    }
}