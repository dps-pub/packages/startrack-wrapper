<?php namespace DPS\StarTrack\Controllers;

use DPS\StarTrack\Exceptions\StarTrackException;
use DPS\StarTrack\Models\Item;
use DPS\StarTrack\Models\Order;
use DPS\StarTrack\Models\Settings;
use DPS\StarTrack\Models\Shipment;

class OrderController extends Controller
{
    /**
     * The order id you wish to retrieve the summary for
     *
     * @param Order $order
     * @return mixed pdf
     * @throws StarTrackException
     */
    public function summary(Order $order)
    {
        $url = "accounts/".$this->account_number."/orders/{$order->order_id}/summary";
        return $this->request(self::GET, $url, [], true);
    }

    public function account()
    {
        return $this->request(self::GET, "accounts/{$this->account_number}");
    }

    public function get($id)
    {
//        dd('todo finish this to return a model');

        return $this->request(self::GET, "orders/{$id}");
    }

    /**
     * @param $suburb
     * @param $state
     * @param $postcode
     *
     * @return boolean
     */
    public function validateSuburb($suburb, $state, $postcode)
    {
        $result = false;

        try{
           $request = $this->request(self::GET, "address?suburb={$suburb}&state={$state}&postcode={$postcode}");
           $result = $request->found;
        } catch(StarTrackException $e){
            // was not found
        }

        return $result;
    }

    /**
     * Track the progress of delivery - rate limited, 10 per minute
     *
     * @param Item $item
     * @return bool|mixed
     *
     * @throws StarTrackException
     */
    public function track(Item $item)
    {
        if(!Settings::rateLimited('tracking', 10)){
            throw new StarTrackException("Tracking limited to 10 per minute, please try again later.");
        }

        return $this->request(self::GET, "track?tracking_ids={$item->article_id}");
    }

    /**
     * @param Order $order
     * @return Order
     * @throws StarTrackException
     */
    public function create($order)
    {
        $response = $this->request(self::PUT, "orders", $order->formatForRequest());

        $order->order_id = $response->order->order_id;
        $order->order_reference = $response->order->order_reference;
        $order->total_cost = $response->order->order_summary->total_cost;
        $order->total_gst = $response->order->order_summary->total_gst;
        $order->status = $response->order->order_summary->status;
        $order->save();

        if(isset($response->order->shipments)){
            foreach($response->order->shipments as $shipmentResponse){

                $shipment = Shipment::where('shipment_id', $shipmentResponse->shipment_id)->first();
                if(!$shipment) $shipment = new Shipment();

                $shipment->shipment_id = $shipmentResponse->shipment_id;
                $shipment->total_cost = isset($shipmentResponse->shipment_summary->total_cost) ? $shipmentResponse->shipment_summary->total_cost : null;
                $shipment->total_gst = isset($shipmentResponse->shipment_summary->total_gst) ? $shipmentResponse->shipment_summary->total_gst : null;
                $shipment->status = isset($shipmentResponse->shipment_summary->status) ? $shipmentResponse->shipment_summary->status : null;
                $shipment->order_id = $order->id;
                $shipment->save();
            }
        }

        return $order->fresh('shipments');
    }
}