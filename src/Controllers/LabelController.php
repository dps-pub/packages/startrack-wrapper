<?php namespace DPS\StarTrack\Controllers;

use DPS\StarTrack\Exceptions\StarTrackException;
use DPS\StarTrack\Models\Label;
use DPS\StarTrack\Models\Shipment;

/**
 * Created by PhpStorm.
 * User: benjamin
 * Date: 28/04/17
 * Time: 9:36 AM
 */
class LabelController extends Controller
{
    public function get(Shipment $shipment)
    {
        if(is_null($shipment->label)){
            throw new StarTrackException("Shipment must have a label created first");
        }

        $response = $this->request(self::GET, "labels/{$shipment->label->request_id}");

        $shipment->label->status = $response->labels[0]->status;

        if(isset($response->labels[0]->url) && $shipment->label->available()){
            $shipment->label->url = $response->labels[0]->url;
            $shipment->label->save();
        }

        return $shipment;
    }

    /**
     * @param Shipment[] $shipments
     * @return Shipment[]
     */
    public function create($shipments)
    {
        $label = new Label();

        foreach($shipments as $shipment){
            $label->addShipment($shipment->shipment_id);
        }

        $response = $this->request(self::POST, "labels", $label->formatForRequest());

        $label->request_id = $response->labels[0]->request_id;
        $label->status = $response->labels[0]->status;
        $label->save();

        foreach($shipments as $shipment) {
            $shipment->label_id = $label->id;
            $shipment->save();
        }

        return $shipments;
    }
}