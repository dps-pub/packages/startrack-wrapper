<?php namespace DPS\StarTrack\Models;

class Order extends Model
{
    public $table = 'StarTrack_Orders';

    public $rules = [
        'order_reference' => 'required|max:50',
    ];

    public function shipments()
    {
        return $this->hasMany(Shipment::class, 'order_id', 'id');
    }

    public function addShipment($shipment)
    {
        if($shipment instanceof Shipment){
            $this->shipments[] = $shipment;
        } else {
            $this->shipments[] = (object)['shipment_id' => $shipment];
        }

        return $this;
    }

    public function formatForRequest()
    {
        parent::formatForRequest();

        return json_encode((object)['order_reference' => $this->order_reference, 'payment_method' => "CHARGE_TO_ACCOUNT", 'shipments' => $this->shipments]);
    }
}