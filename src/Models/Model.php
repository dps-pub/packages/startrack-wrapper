<?php namespace DPS\StarTrack\Models;

use DPS\StarTrack\Exceptions\StarTrackException;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model as BaseModel;

class Model extends BaseModel
{
    use SoftDeletes;

    public $fillable = ['*'];

    protected $dateFormat = 'Y-m-d H:i:s';

    public function __construct(array $attributes = array())
    {
        parent::__construct($attributes);
        $this->validator = \App::make('validator');

        $this->connection = config('startrack.database');
    }

    /**
     * Error message bag
     *
     * @var Illuminate\Support\MessageBag
     */
    protected $errors;

    /**
     * Validation rules
     *
     * @var []
     */
    public $rules = [];

    /**
     * Validator instance
     *
     * @var Illuminate\Validation\Validators
     */
    protected $validator;

    /**
     * Listen for save event
     */
    protected static function boot()
    {
        parent::boot();

//        static::saving(function($model){
//            return $model->validate();
//        });
    }

    /**
     * Validates current attributes against rules
     */
    public function validate()
    {
        $v = $this->validator->make($this->attributes, $this->rules);

        if($v->passes()){
            return true;
        }

        $this->setErrors($v->messages());

        return false;
    }

    /**
     * Set error message bag
     *
     * @var Illuminate\Support\MessageBag
     */
    protected function setErrors($errors)
    {
        $this->errors = $errors;
    }

    /**
     * Retrieve error message bag
     */
    public function getErrors()
    {
        return $this->errors;
    }

    /**
     * Inverse of wasSaved
     */
    public function hasErrors()
    {
        return !empty($this->errors);
    }

    /**
     * Unset's properties on the model that are null
     */
    public function removeNulls()
    {
        foreach($this->attributes as $name => $value){
            if($this->isNull($value)){
                unset($this->{$name});
            }
        }

        return $this;
    }

    public function formatForRequest()
    {
        if (!$this->validate()) {
            throw new StarTrackException($this->getErrors());
        }

        $this->removeNulls();
    }

    public function isNull($value)
    {
        return is_null($value) || $value == "";
    }

    public function isVeryDirty()
    {
        return parent::isDirty() || $this->exists;
    }
}