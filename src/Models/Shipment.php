<?php namespace DPS\StarTrack\Models;

use DPS\StarTrack\Exceptions\StarTrackException;

class Shipment extends Model
{
    public $table = 'StarTrack_Shipments';

    public $rules = [
        'shipment_reference' => 'required|max:50',
    ];

    public function items()
    {
        return $this->hasMany(Item::class, 'shipment_id', 'id');
    }

    public function to()
    {
        return $this->hasOne(Address::class, 'id', 'address_to_id');
    }

    public function from()
    {
        return $this->hasOne(Address::class, 'id', 'address_from_id');
    }

    public function label()
    {
        return $this->hasOne(Label::class, 'id', 'label_id');
    }

    public function hasAddresses()
    {
        return ((bool)$this->to && (bool)$this->from);
    }

    public function hasLabel()
    {
        return (bool) $this->label;
    }

    public function hasAvailableLabels()
    {
        return $this->hasLabel() && $this->label->available();
    }

    public function createLabel()
    {
        $label = new Label();
        $label->addShipment($this->shipment_id);

        return $label->formatForRequest();
    }

    public function formatForRequest()
    {
        parent::formatForRequest();
        $shipment = $this;

        if(isset($shipment->despatch_date)){
            $dispatch = new \DateTime($this->despatch_date);
            $dispatchFuture = (new \DateTime())->modify('+14 days');

            if($dispatch >= $dispatchFuture){
                throw new StarTrackException('Dispatch date must be within 14 days of Today');
            }

            $shipment->despatch_date = $shipment->despatch_date->format("Y-m-d");
        }

        $shipment->to->formatForRequest();
        $shipment->from->formatForRequest();
        $shipment->consolidate = false;
        $shipment->email_tracking_enabled = false;

        $items = [];

        foreach($shipment->items as $item){
            $items[] = $item->formatForRequest();
        }

        return $shipment;
    }

}