<?php namespace DPS\StarTrack\Models;

/**
 * DPS\StarTrack\Models\Item
 *
 * @property-read \DPS\StarTrack\Models\Shipment $shipment
 * @mixin \Eloquent
 */
class Item extends Model
{
    public $table = 'StarTrack_Items';

    public $rules = [
        'item_reference' => 'max:50',
        'product_id' => 'required|max:5',
        'invoice_number' => 'max:35',
        'packaging_type' => 'required|in:CTN,PAL,SAT,BAG,ENV,ITM,JIF,SKI',
        'length' => 'required|max:5',
        'height' => 'required|max:5',
        'width' => 'required|max:7',
        'weight' => 'required:4',
    ];

    public function shipment()
    {
        return $this->belongsTo(Shipment::class, 'shipment_id', 'id');
    }

    public function formatForRequest()
    {
        parent::formatForRequest();

        return $this;
    }
}