<?php namespace DPS\StarTrack\Models;

class Price
{
    private $from;
    private $to;
    private $items;

    public function __construct($fromPostcode, $fromSuburb, $toPostcode, $toSuburb, $length, $height, $width, $weight, $quantity = 1)
    {
        $this->from = (object)[];
        $this->from->postcode = $fromPostcode;
        $this->from->suburb = $fromSuburb;

        $this->to = (object)[];
        $this->to->postcode = $toPostcode;
        $this->to->suburb = $toSuburb;

        $item = (object)[];
        $item->length = $length;
        $item->height = $height;
        $item->width = $width;
        $item->weight = $weight;
        $item->item_reference = str_random();

        $this->items = [];
        for($i = 0; $i < $quantity; $i++){
            $this->items[] = $item;
        }
    }

    public function formatForRequest()
    {
        return json_encode(["from" => $this->from, "to" => $this->to, "items" => $this->items]);
    }
}