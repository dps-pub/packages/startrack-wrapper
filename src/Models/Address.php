<?php

namespace DPS\StarTrack\Models;

use Illuminate\Support\Str;

/**
 * DPS\StarTrack\Models\Address
 *
 * @property-read \DPS\StarTrack\Models\Shipment $shipment
 *
 * @mixin \Eloquent
 */
class Address extends Model
{
    public $table = 'StarTrack_Addresses';

    public $rules = [
        'name' => 'required|max:40',
        'business_name' => 'max:40',
        'line_1' => 'required|max:40',
        'line_2' => 'max:40',
        'line_3' => 'max:40',
        'suburb' => 'required|max:30',
        'state' => 'required|max:3',
        'postcode' => 'required|max:4',
        'phone' => 'max:10',
        'email' => 'max:50',
        'delivery_instructions' => 'max:75',
    ];

    public function shipment()
    {
        return $this->belongsTo(Shipment::class, 'shipment_id', 'id');
    }

    public function formatForRequest()
    {
        parent::formatForRequest();

        $this->lines = [$this->line_1];

        if (isset($this->line_2)) {
            $this->lines = array_merge($this->lines, [$this->line_2]);
        }

        if (isset($this->line_3)) {
            $this->lines = array_merge($this->lines, [$this->line_3]);
        }

        unset($this->line_1, $this->line_2, $this->line_3);

        // split delivery_instructions into an array of strings
        if (blank($this->delivery_instructions)) {
            unset($this->delivery_instructions);
        } else {
            $this->delivery_instructions = Str::of($this->delivery_instructions)
                ->explode("\n")
                ->toArray();
        }

        return json_encode($this);
    }
}
