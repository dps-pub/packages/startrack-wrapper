<?php namespace DPS\StarTrack\Models;

class Settings extends Model
{
    public $table = 'StarTrack_Settings';

    public static function rateLimited($name, $allowedPerMin)
    {
        $result = Settings::whereName($name)->first();
        if(!$result){
            $data = (object)[];
            $data->count = 0;
            $data->created_at = date('Y-m-d H:i:s');

            $result = new Settings();
            $result->name = $name;
            $result->data = json_encode($data);
        }


        $data = json_decode($result->data);
        $data->count++;

        $date = new \DateTime($data->created_at);
        $now = new \DateTime();

        $diff = intval(($date->diff($now))->format('%I'));

        if($diff >= 1){
            $data->count = 1;
            $data->created_at = $now;
        } else {
            if($data->count > $allowedPerMin){
                return false;
            }
        }

        $result->data = json_encode($data);
        $result->save();

        return true;
    }

}