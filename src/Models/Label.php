<?php namespace DPS\StarTrack\Models;

use DPS\StarTrack\Exceptions\StarTrackException;

class Label extends Model
{
    public $table = 'StarTrack_Labels';

    public $preferences;
    public $shipments;

    private function setPreferences()
    {
        $this->preferences = [
            (object)[
                'type' => 'PRINT',
                'groups' => [
                    (object)[
                        "group" => "StarTrack",
                        "layout" => "A4-2pp landscape",
                        "branded" => true,
                        "left_offset" => 49,
                        "top_offset" => 10
                    ]
                ]
            ]
        ];
    }

    public function addShipment($id)
    {
        $this->shipments[] = (object)['shipment_id' => $id];

        return $this;
    }

    public function formatForRequest()
    {
        $this->setPreferences();

        if(empty($this->shipments)){
            throw new StarTrackException("Please set shipment id(s)");
        }

        return json_encode((object)['preferences' => $this->preferences, 'shipments' => $this->shipments]);
    }

    public function available()
    {
        return $this->status == "AVAILABLE";
    }
}