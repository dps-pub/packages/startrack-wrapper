<?php namespace DPS\StarTrack;

use DPS\StarTrack\Controllers\LabelController;
use DPS\StarTrack\Controllers\OrderController;
use DPS\StarTrack\Controllers\PostageController;
use DPS\StarTrack\Controllers\ShipmentController;

class StarTrack
{
    public $label;
    public $order;
    public $shipment;
    public $postage;

    function __construct()
    {
        $this->label = new LabelController();
        $this->order = new OrderController();
        $this->shipment = new ShipmentController();
        $this->postage = new PostageController();

        // Australia Post’s networks and systems are designed to accept one order per day
        // An order cannot be cancelled, deleted or voided online.
        // A shipment can be deleted before it is added to an order but once added to an order neither the shipment within the order nor the order can be deleted.
    }

}
