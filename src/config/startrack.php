<?php

return [
    'api_key' => env('STARTRACK_API'),

    'secret' => env('STARTRACK_SECRET'),

    'postage_api_key' => '',

    'account_number' => env('STARTRACK_ACCOUNT_NUMBER'),

    'database' => config('database.default'),

    'debug' => false
];
