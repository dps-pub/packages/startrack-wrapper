# StarTrack API Wrapper

## Requirements

Laravel 5.

## Installation

You can install the package using the [Composer](https://getcomposer.org/) package manager. You can install it by running this command in your project root:

```bash
composer require dps/star-track
```

Add the `DPS\StarTrack\StarTrackServiceProvider` provider to the `providers` array in `config/app.php`:

```php
'providers' => [
  ...
  DPS\StarTrack\ServiceProvider::class,
],
```

Finally, publish the config file with `php artisan vendor:publish`. You'll find it at `config/startrack.php`.

## Configuration

The config file comes with defaults and placeholders. 

## Usage

